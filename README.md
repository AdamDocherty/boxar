# README #

## User Guide ##

The game requires two players: one player controls a virtual character using the PlayStation®Vita, while the other plays the role of the opponent and wears the AR headgear and gloves. 
For the rest of this document, these players will be referred to as "the player" and "the opponent" respectively.
The goal for the player is to knock out the opponent by punching them enough times in the face. 
The player can punch using the Square and Circle buttons for the left and right arm respectively. 
As the player deals more damage to the opponent, the damage is visualised as bruises on the opponents face. 
Additionally, the player can block attacks made by the opponent by using the L and R shoulder buttons, blocking an attack however will disable the use of that arm for a brief period of time.

The goal for the opponent is to knock out the player. 
Each AR glove has two markers, one on the back of the hand and one on the fingers. 
By raising their fist into a blocking position, the back marker is shown and the opponent is considered blocking. 
Blocking a punch from the player before it hits your face will prevent any damage. 
By punching outwards towards the PS Vita’s camera, the finger marker is shown and the opponent is considered attacking.
Punching towards the camera while the player is not blocking will result in a hit (the punch should finish around 20cm away from the PS Vita camera). 
Feedback is given to the opponent via audio cues.

![ar-gear.jpg](https://bitbucket.org/repo/nyjp6o/images/1199966724-ar-gear.jpg)

![game-screenshot.png](https://bitbucket.org/repo/nyjp6o/images/3103726651-game-screenshot.png)