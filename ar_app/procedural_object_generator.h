#ifndef procedural_object_generator_h__
#define procedural_object_generator_h__

#pragma once

namespace abfw {
	class Mesh;
	class Platform;
	class Texture;
}

class ProceduralObjectGenerator
{
private:
	// Make this a static class
	ProceduralObjectGenerator() = delete;
	~ProceduralObjectGenerator() = delete;
	ProceduralObjectGenerator(const ProceduralObjectGenerator& copy) = delete;
	ProceduralObjectGenerator operator=(const ProceduralObjectGenerator& copy) = delete;

public:
	// creates a glove mesh, the palm of the glove will be facing towards (0, 0, -1)
	static abfw::Mesh* CreateGloveMesh(abfw::Platform& platform, const float width, 
		const float height, const float depth, const int side);

	// creates a plane mesh on the xy plane
	static abfw::Mesh* CreatePlaneMesh(abfw::Platform& platform, const float width,
		const float height);
};

#endif // procedural_object_generator_h__
