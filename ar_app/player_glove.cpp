#include "player_glove.h"

#include "audio.h"

namespace {

	// The punch timer is used for both the forward punching motion and 
	// the return punching motion.

	// Time taken for the punch to reach the furthest point and return
	const float kPunchDuration = 0.5f;
	// Time taken for the punch to reach the furthest point
	const float kPunchPeakDuration = 0.2f;

	const float kPunchDistance = 0.5f;

	// Time taken before the block starts in seconds
	const float kBlockDelay = 0.2f;
	// The time taken for the glove to recover from blocking an attack in seconds
	const float kBlockCooldown = 0.8f;
}

PlayerGlove::PlayerGlove(abfw::Model& model, const abfw::Vector3& resting_position)
	: Glove(model)
	, resting_position_(resting_position)
	, blocking_(0.0f)
	, punch_timer_(0.0f)
	, block_timer_(0.0f)
	, block_cooldown_timer_(0.0f)
{
}

PlayerGlove::~PlayerGlove()
{
}

void PlayerGlove::Punch()
{
	// only punch if the player is not currently punching
	if (punch_timer_ <= 0.0f)
	{
		Audio::GetInstance().PlaySample("swing");
		punch_timer_ = kPunchDuration;
	}
}

void PlayerGlove::Block(bool block)
{
	// only set blocking to true if the player is not punching and
	// the block is not on cooldown
	if (block && punch_timer_ <= 0.0f && block_cooldown_timer_ <= 0.0f)
	{
		blocking_ = true;
	}
	else
	{
		blocking_ = false;
	}
}

void PlayerGlove::Stop()
{
	if (IsPunching()) {
		const float return_duration = kPunchDuration - kPunchPeakDuration;
		// set the punch timer to be in the `return' part of the timer.
		// the position of the glove should not change, only the direction is reversed
		punch_timer_ = (return_duration) *
			(1.0f - ((punch_timer_ - (return_duration)) / kPunchPeakDuration));
	}
}

void PlayerGlove::BlockCooldown()
{
	block_cooldown_timer_ = kBlockCooldown;
}

bool PlayerGlove::IsPunching() const
{
	// player is only punching in the first part of the punch timer
	// i.e. when kPunchDuration < punch_timer_ < (kPunchDuration - kPunchPeakDuration)
	return punch_timer_ + kPunchPeakDuration > kPunchDuration;
}

bool PlayerGlove::IsBlocking() const
{
	// the player is only blocking if they have been holding the block button for more
	// than kBlockDelay seconds
	return (block_timer_ >= kBlockDelay);
}

void PlayerGlove::Update(const float time_elapsed)
{
	// update relevant timers
	if (block_cooldown_timer_ > 0.0f)
		block_cooldown_timer_ -= time_elapsed;

	if (blocking_) {
		block_timer_ += time_elapsed;
	} else {
		block_timer_ -= time_elapsed;
	}

	// clamp the block_timer_ between 0.0f and block_timer_
	block_timer_ = std::max(std::min(block_timer_, kBlockDelay), 0.0f);

	UpdateAnimation(time_elapsed);
}

void PlayerGlove::UpdateAnimation(const float time_elapsed)
{
	if (punch_timer_ > 0.0f) 
	{
		punch_timer_ -= time_elapsed;
		float t = 0.0f;
		if (punch_timer_ < kPunchDuration - kPunchPeakDuration) {
			t = punch_timer_ / (kPunchDuration - kPunchPeakDuration);
		}
		else
		{
			t = 1.0f - ((punch_timer_ - kPunchPeakDuration) / (kPunchDuration - kPunchPeakDuration));
		}
		SetPositionPunching(t);
	}
	else if (block_timer_ > 0.0f) 
	{
		float t = block_timer_ / kBlockDelay;
		SetPositionBlocking(t);
	}
	else
	{
		SetPositionResting();
	}
}

void PlayerGlove::SetPositionPunching(float t)
{
	// rotate the mesh_instance so that the fist is pointing away from the camera
	abfw::Matrix44 x_rotation;
	x_rotation.RotationX(1.57079632679f);
	abfw::Matrix44 y_rotation;
	y_rotation.RotationY(3.14159265359f);

	abfw::Matrix44 transformation_matrix = x_rotation * y_rotation;

	const abfw::Vector3 translation = resting_position_ +
		abfw::Vector3(0.0f, 0.1f, -kPunchDistance * t);

	transformation_matrix.SetTranslation(translation);

	mesh_instance_.set_transform(transformation_matrix);
}

void PlayerGlove::SetPositionBlocking(float t)
{
	const abfw::Vector3 translation = resting_position_ +
		abfw::Vector3(0.0f, 0.1f * t, 0.0f);

	abfw::Matrix44 transformation_matrix;
	transformation_matrix.SetIdentity();
	transformation_matrix.SetTranslation(translation);

	mesh_instance_.set_transform(transformation_matrix);
}

void PlayerGlove::SetPositionResting()
{
	abfw::Matrix44 transformation_matrix;
	transformation_matrix.SetIdentity();
	transformation_matrix.SetTranslation(resting_position_);

	mesh_instance_.set_transform(transformation_matrix);
}
