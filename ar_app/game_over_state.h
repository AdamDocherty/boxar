#ifndef game_over_state_h__
#define game_over_state_h__

#pragma once

#include "state.h"

#include <memory>

namespace abfw {
	class SpriteRenderer;
	class Font;
}

class GameOverState : public State
{
public:
	// set win to true if the player has won
	// false if the opponent has won
	GameOverState(abfw::Platform& platform, bool win);
	~GameOverState();

	virtual bool Init();
	virtual bool HandleInput(abfw::SonyControllerInputManager* input_manager);
	virtual bool Update(float frame_time);
	virtual bool Render();
private:

	std::shared_ptr<abfw::SpriteRenderer> sprite_renderer_;
	std::shared_ptr<abfw::Font> font_;

	bool win_;

};

#endif // game_over_state_h__
