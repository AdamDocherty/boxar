#ifndef constants_h__
#define constants_h__


namespace Constants {

	// The size of the markers in meters
	const float kSonyDefaultMarkerSize = 0.059f;
	const float kMarkerSize = 0.044f; 
}

#endif // constants_h__