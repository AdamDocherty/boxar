#ifndef opponent_face_h__
#define opponent_face_h__

#pragma once

#include <graphics/mesh_instance.h>
#include <maths/sphere.h>

namespace abfw {
	class Model;
}

class OpponentFace
{
public:
	OpponentFace(abfw::Model& model, const int marker_index);
	~OpponentFace();

	// returns true if the face is on screen
	inline bool IsOnScreen() const { return on_screen_; }

	void Update(const float time_elapsed);

	inline abfw::MeshInstance& mesh_instance() { return mesh_instance_; }
	inline const abfw::MeshInstance& mesh_instance() const { return mesh_instance_; }

	inline const abfw::Sphere& collision_sphere() const { return collision_sphere_; }

private:
	// offsets the face model from the marker and positions
	// the collision sphere to be over the opponents head
	void PositionFace(abfw::Matrix44 marker_transform);

	abfw::MeshInstance mesh_instance_;
	abfw::Sphere collision_sphere_;

	const int marker_index_;
	bool on_screen_;
};

#endif // opponent_face_h__
