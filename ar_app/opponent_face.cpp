#include "opponent_face.h"

#include "constants.h"
#include "ar_helper.h"
#include <graphics/model.h>
#include <graphics/mesh.h>

OpponentFace::OpponentFace(abfw::Model& model, const int marker_index)
	: marker_index_(marker_index)
	, on_screen_(false)
{
	mesh_instance_.set_mesh(model.mesh());
	collision_sphere_.set_radius(mesh_instance_.mesh()->aabb().max_vtx().x);
}

OpponentFace::~OpponentFace()
{
}

void OpponentFace::Update(const float time_elapsed)
{
	on_screen_ = false;

	abfw::Matrix44 marker_transform;
	if (ARHelper::GetMarkerTransform(marker_transform, marker_index_, Constants::kMarkerSize)) {
		on_screen_ = true;
		PositionFace(marker_transform);
	}
}

void OpponentFace::PositionFace(abfw::Matrix44 marker_transform)
{
	// Move the matrix so that the front of the virtual glove matches the front of the real glove
	abfw::Vector3 translation = marker_transform.GetTranslation();
	abfw::Vector3 y_axis = abfw::Vector3(marker_transform.m[1][0], marker_transform.m[1][1], marker_transform.m[1][2]);
	translation -= y_axis * (mesh_instance_.mesh()->aabb().max_vtx().y * 0.75f);
	marker_transform.SetTranslation(translation);

	mesh_instance_.set_transform(marker_transform);

	abfw::Vector3 z_axis = abfw::Vector3(marker_transform.m[2][0], marker_transform.m[2][1], marker_transform.m[2][2]);
	translation -= z_axis * (mesh_instance_.mesh()->aabb().max_vtx().z * 0.75f);

	collision_sphere_.set_position(translation);
}
