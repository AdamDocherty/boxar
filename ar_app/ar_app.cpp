#include "ar_app.h"

#include <system/platform.h>
#include <input/sony_controller_input_manager.h>
#include <maths/matrix44.h>
#include <graphics/sprite_renderer.h>
#include <graphics/font.h>

#include "audio.h"
#include "menu_state.h"

ARApp* ARApp::s_instance;

ARApp::ARApp(abfw::Platform& platform) 
	: Application(platform)
	, input_manager_(NULL)
	, state_(nullptr)
	, next_state_(new MenuState(platform))
{
	s_instance = this;
	input_manager_ = platform_.CreateSonyControllerInputManager();
}

ARApp::~ARApp() {}

void ARApp::Init()
{
	// Initialise the audio manager
	Audio::GetInstance().set_platform(&platform_);
	Audio::GetInstance().set_audio_manager(platform_.CreateAudioManager());

	// initialises the sprite renderer
	// all states will get the sprite renderer from here
	sprite_renderer_.reset(platform_.CreateSpriteRenderer());
	abfw::Matrix44 proj_matrix2d;
	proj_matrix2d.OrthographicFrustumVita(0.0f, platform_.width(), 0.0f, platform_.height(), -1.0f, 1.0f);
	sprite_renderer_->set_projection_matrix(proj_matrix2d);

	// initialises the 3d renderer
	// all states will get the 3d renderer from here
	renderer_3d_.reset(platform_.CreateRenderer3D());

	// initialises the font
	// all states will get the font from here
	font_.reset(new abfw::Font());
	font_->Load("comic_sans", platform_);
}

void ARApp::CleanUp()
{
	Audio::GetInstance().CleanUp();

	delete input_manager_;
	input_manager_ = NULL;
}

bool ARApp::Update(float frame_time)
{
	// transition to next state if it exists
	if (next_state_ != nullptr)
	{
		state_.reset(next_state_);
		if (state_.get()) state_->Init();
		next_state_ = nullptr;
	}

	HandleInput();
	if (state_.get()) state_->Update(frame_time);

	return true;
}

void ARApp::HandleInput()
{
	if(input_manager_)
	{
		input_manager_->Update();
		if (state_.get()) state_->HandleInput(input_manager_);
	}
}

void ARApp::Render()
{
	if (state_.get()) state_->Render();
}

