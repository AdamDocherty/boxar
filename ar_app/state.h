#ifndef state_h__
#define state_h__

#pragma once

namespace abfw {
	class Platform;
	class SonyControllerInputManager;
	class Renderer3D;
	class SpriteRenderer;
	class Font;
}

// Base class for all game states
class State
{
public:
	explicit State(abfw::Platform& platform);
	virtual ~State() {}

	virtual bool Init() = 0;

	// Input Methods
	virtual bool HandleInput(abfw::SonyControllerInputManager* input_manager) = 0;

	// Update Methods
	virtual bool Update(float frame_time) = 0;

	// Render Methods
	virtual bool Render() = 0;

protected:
	abfw::Platform& platform_;
};

#endif // state_h__
