#include "menu_state.h"

#include <system/platform.h>
#include <graphics/sprite_renderer.h>
#include <graphics/font.h>
#include <input/sony_controller_input_manager.h>
#include <maths/vector3.h>

#include "ar_app.h"
#include "game_state.h"
#include "help_state.h"

MenuState::MenuState(abfw::Platform& platform)
	: State(platform)
{
}

MenuState::~MenuState()
{
}

bool MenuState::Init()
{
	sprite_renderer_ = ARApp::GetInstance()->sprite_renderer();
	font_ = ARApp::GetInstance()->font();

	return true;
}

bool MenuState::HandleInput(abfw::SonyControllerInputManager* input_manager)
{
	const abfw::SonyController* controller = input_manager->GetController(0);

	if(controller)
	{
		if(controller->buttons_pressed() & ABFW_SONY_CTRL_START)
		{
			ARApp::GetInstance()->TransitionTo(new GameState(platform_));
		}

		else if (controller->buttons_pressed() & ABFW_SONY_CTRL_SQUARE)
		{
			ARApp::GetInstance()->TransitionTo(new HelpState(platform_));
		}
	}

	return true;
}

bool MenuState::Update(float frame_time)
{
	return true;
}

bool MenuState::Render()
{
	sprite_renderer_->Begin(true);

	// render menu text
	if (font_.get()) {
		font_->RenderText(sprite_renderer_.get(), abfw::Vector3(480.0f, 100.0f, -0.9f), 4.0f,
			0xffffffff, abfw::TJ_CENTRE, "BoxAR");
		font_->RenderText(sprite_renderer_.get(), abfw::Vector3(480.0f, 300.0f, -0.9f), 2.0f,
			0xffffffff, abfw::TJ_CENTRE, "Press Start to begin.");
		font_->RenderText(sprite_renderer_.get(), abfw::Vector3(480.0f, 400.0f, -0.9f), 2.0f,
			0xffffffff, abfw::TJ_CENTRE, "Press Square for Help.");
	}

	sprite_renderer_->End();

	return true;
}
