#ifndef _RENDER_TARGET_APP_H
#define _RENDER_TARGET_APP_H

#include <system/application.h>

// language includes
#include <memory>

namespace abfw
{
	class Platform;
	class SonyControllerInputManager;
	class Renderer3D;
	class SpriteRenderer;
	class Font;
}

class State;

class ARApp : public abfw::Application
{
public:
	ARApp(abfw::Platform& platform);
	~ARApp();

	// Get the current instance of the application
	static ARApp* GetInstance() { return s_instance; }
	// Transitions to the new state after the current frame is over
	void TransitionTo(State * next_state) { next_state_ = next_state; }

	void Init();
	void CleanUp();
	bool Update(float frame_time);
	void Render();

	std::shared_ptr<abfw::Renderer3D> renderer_3d() { return renderer_3d_; }
	std::shared_ptr<abfw::SpriteRenderer> sprite_renderer() { return sprite_renderer_; }
	std::shared_ptr<abfw::Font> font() { return font_; }
private:
	void HandleInput();

	// stores the current instance of the application
	static ARApp* s_instance;

	abfw::SonyControllerInputManager* input_manager_;

	std::shared_ptr<abfw::Renderer3D> renderer_3d_;
	std::shared_ptr<abfw::SpriteRenderer> sprite_renderer_;
	std::shared_ptr<abfw::Font> font_;

	std::unique_ptr<State> state_;
	// if not null, will transition to next_state_ at the start of the next frame
	State * next_state_;
};




#endif // _RENDER_TARGET_APP_H