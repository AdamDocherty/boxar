#include "audio.h"

#include <system/platform.h>
#include <audio/audio_manager.h>

#include <string>

Audio::Audio()
{
}

Audio::~Audio()
{
}

void Audio::CleanUp()
{
	audio_manager_.release();
	platform_ = nullptr;
}

void Audio::set_audio_manager(abfw::AudioManager* audio_manager)
{
	audio_manager_.reset(audio_manager);
}

void Audio::PlaySample(const std::string& name)
{
	// find the sample id based on the name
	// if the name is not already in the map, load the sample
	int sample_id = 0;
	auto it = sample_map_.find(name);
	if (it != sample_map_.end()) {
		sample_id = it->second;
	} else {
		sample_id = LoadSample(name);
	}
	audio_manager_->PlaySample(sample_id);
}

int Audio::LoadSample(const std::string& name)
{
	std::string filename = "res/";
	filename += name;
	filename += ".wav";
	int sample_id = audio_manager_->LoadSample(filename.c_str(), *platform_);

	sample_map_[name] = sample_id;
	return sample_id;
}

