#ifndef opponent_glove_h__
#define opponent_glove_h__

#pragma once

#include "glove.h"

#include <deque>

class OpponentGlove : public Glove
{
public:
	OpponentGlove(abfw::Model& model, int marker_index_attacking, int marker_index_defending);
	~OpponentGlove();

	virtual void Update(const float time_elapsed);
	// call when the opponent has punched to start the punch cooldown timer
	void Punch();
	// call when the opponent has successfully blocked a punch from the player
	void Blocked();

	// returns true if the glove is on screen
	inline bool IsOnScreen() const { return IsAttacking() || IsDefending(); }
	// returns true if the glove is the attacking/punching position
	inline bool IsAttacking() const { return attacking_; }
	// returns true if the glove is in the defending/blocking position
	inline bool IsDefending() const { return defending_; }
	// returns true if the opponent is currently punching
	bool IsPunching();

private:
	void GetStateFromMarkers();
	// offsets the mesh_instance so that the front of the glove lines up with the marker
	void SetAttacking(abfw::Matrix44& marker_transform);
	// offsets the mesh_instance so that the back of the glove lines up with the marker
	void SetDefending(abfw::Matrix44& marker_transform);

	// saves the current position of the marker
	void SavePosition(const abfw::Vector3& pos);
	const int marker_index_attacking_;
	const int marker_index_defending_;
	bool attacking_;
	bool defending_;

	float punch_cooldown_timer_;

	// stores all previous positions of the marker
	// used to determine if the opponent is punching
	std::deque<abfw::Vector3> previous_positions_;
};

#endif // opponent_glove_h__
