#include "game_state.h"

#include <system/platform.h>
#include <graphics/sprite_renderer.h>
#include <assets/png_loader.h>
#include <graphics/image_data.h>
#include <graphics/font.h>
#include <input/touch_input_manager.h>
#include <maths/vector2.h>
#include <input/sony_controller_input_manager.h>
#include <maths/math_utils.h>
#include <graphics/renderer_3d.h>
#include <graphics/mesh.h>
#include <graphics/primitive.h>
#include <graphics/render_target.h>
#include <graphics/vita/texture_vita.h>

#include <sony_sample_framework.h>
#include <sony_tracking.h>

#include "ar_app.h"
#include "game_over_state.h"

#include "constants.h"
#include "audio.h"
#include "ar_helper.h"
#include "procedural_object_generator.h"
#include "collision_detection.h"

#include "player.h"
#include "opponent.h"

void *graphicsAlloc(SceKernelMemBlockType type, uint32_t size, uint32_t alignment, uint32_t attribs, SceUID *uid);
void graphicsFree(SceUID uid);

std::unique_ptr<abfw::TextureVita> GameState::s_camera_feed_texture;

GameState::GameState(abfw::Platform& platform)
	: State(platform)
	, fps_(0.0f)
	, render_debug_info_(false)
{
}

GameState::~GameState()
{
	smartRelease();
	sampleRelease();
}

bool GameState::Init()
{
	if (!s_camera_feed_texture.get()) {
		// only create the camera feed texture once
		// since it is a smart pointer, it will be deleted when the application closes
		s_camera_feed_texture.reset(new abfw::TextureVita());
	}

	sprite_renderer_ = ARApp::GetInstance()->sprite_renderer();
	renderer_3d_ = ARApp::GetInstance()->renderer_3d();

	InitFont();

	// initialise sony framework
	sampleInitialize();
	smartInitialize();

	// reset marker tracking
	AppData* dat = sampleUpdateBegin();
	smartTrackingReset();
	sampleUpdateEnd(dat);

	InitCameraFeed();
	Init3dProjectionMatrix();
	view_matrix_3d_.SetIdentity();

	// Load models
	LoadModels();

	opponent_.reset(new Opponent(models_["opponent_left_glove"], models_["opponent_right_glove"], models_["face"]));
	player_.reset(new Player(models_["player_left_glove"], models_["player_right_glove"]));

	return true;
}

bool GameState::HandleInput(abfw::SonyControllerInputManager* input_manager)
{
	const abfw::SonyController* controller = input_manager->GetController(0);

	if(controller)
	{
		if(controller->buttons_pressed() & ABFW_SONY_CTRL_SELECT)
		{
			cameraInitialize();
		}

		if(controller->buttons_pressed() & ABFW_SONY_CTRL_TRIANGLE)
		{
			render_debug_info_ = !render_debug_info_;
		}

		player_->HandleInput(controller);
	}

	return true;
}

bool GameState::Update(float frame_time)
{
	fps_ = 1.0f / frame_time;

	UpdateAR(frame_time);

	HandleCollisions();

	CheckGameOverConditions();

	return true;
}

bool GameState::Render()
{
	AppData* dat = sampleRenderBegin();

	RenderCameraFeed(dat);

	Render3D();

	RenderSprites();

	sampleRenderEnd();

	return true;
}

bool GameState::UpdateAR(float frame_time)
{
	AppData* dat = sampleUpdateBegin();
	smartUpdate(dat->currentImage);

	player_->Update(frame_time);
	opponent_->Update(frame_time);

	sampleUpdateEnd(dat);

	return true;
}

bool GameState::Render3D()
{
	renderer_3d_->set_projection_matrix(projection_matrix_3d_);
	renderer_3d_->set_view_matrix(view_matrix_3d_);

	// Begin rendering 3D meshes, don't clear the frame buffer
	renderer_3d_->Begin(false);

	player_->Render(renderer_3d_.get());
	opponent_->Render(renderer_3d_.get());

	renderer_3d_->End();

	return true;
}

bool GameState::RenderSprites()
{
	abfw::Matrix44 proj_matrix2d;
	proj_matrix2d.OrthographicFrustumVita(0.0f, platform_.width(), 0.0f, platform_.height(), -1.0f, 1.0f);
	sprite_renderer_->set_projection_matrix(proj_matrix2d);
	sprite_renderer_->Begin(false);

	DrawHUD();

	sprite_renderer_->End();

	return true;
}

void GameState::RenderCameraFeed(struct AppData* dat)
{
	sprite_renderer_->set_projection_matrix(camera_feed_matrix_);

	sprite_renderer_->Begin(true);

	// draw camera feed sprite
	if(dat->currentImage)
	{
		s_camera_feed_texture->set_texture(dat->currentImage->tex_yuv);
		sprite_renderer_->DrawSprite(camera_feed_sprite_);
	}

	sprite_renderer_->End();
}

void GameState::InitFont()
{
	font_ = ARApp::GetInstance()->font();
}

void GameState::Init3dProjectionMatrix()
{
	abfw::Matrix44 fov_projection_matrix;
	fov_projection_matrix.PerspectiveFovVita(
		SCE_SMART_IMAGE_FOV,
		(float)SCE_SMART_IMAGE_WIDTH / (float)SCE_SMART_IMAGE_HEIGHT,
		0.01f,
		2.0f);
	abfw::Matrix44 scale_factor_matrix;
	scale_factor_matrix.SetIdentity();
	scale_factor_matrix.Scale(abfw::Vector3(1.0f, VIEW_SCALE_V, 1.0f));
	projection_matrix_3d_ = fov_projection_matrix * scale_factor_matrix;
}

void GameState::InitCameraFeed()
{
	camera_feed_matrix_.OrthographicFrustumVita(-1.0f, 1.0f, -1.0f, 1.0f, -1.0f, 1.0f);
	camera_feed_sprite_.set_width(2.0f);
	camera_feed_sprite_.set_height(2.0f * VIEW_SCALE_V );
	camera_feed_sprite_.set_position(0.0f, 0.0f, 1.0f);
	camera_feed_sprite_.set_texture(s_camera_feed_texture.get());
}

void GameState::LoadModels()
{
	models_["opponent_left_glove"].set_mesh(ProceduralObjectGenerator::CreateGloveMesh(platform_, 0.09f, 0.10f, 0.08f, -1));
	models_["opponent_right_glove"].set_mesh(ProceduralObjectGenerator::CreateGloveMesh(platform_, 0.09f, 0.10f, 0.08f, 1));

	models_["player_left_glove"].set_mesh(ProceduralObjectGenerator::CreateGloveMesh(platform_, 0.09f, 0.10f, 0.08f, -1));
	models_["player_right_glove"].set_mesh(ProceduralObjectGenerator::CreateGloveMesh(platform_, 0.09f, 0.10f, 0.08f, 1));

	models_["face"].set_mesh(ProceduralObjectGenerator::CreatePlaneMesh(platform_, 0.2f, 0.3f));
	const std::string texture_name = "res/beaten-up.png";
	SetTextureForModel(&models_["face"], texture_name);
}

void GameState::HandleCollisions()
{
	HandlePlayerGlovePunching(player_->left_glove());
	HandlePlayerGlovePunching(player_->right_glove());
	HandleOpponentGlovePunching(opponent_->left_glove());
	HandleOpponentGlovePunching(opponent_->right_glove());
}

void GameState::HandlePlayerGlovePunching(PlayerGlove& player_glove)
{
	if (player_glove.IsPunching()) {

		if (opponent_->left_glove().IsDefending() &&
			CollisionDetection::CollisionExists(player_glove, opponent_->left_glove())) {
				player_glove.Stop();
				return;
		}

		if (opponent_->right_glove().IsDefending() &&
			CollisionDetection::CollisionExists(player_glove, opponent_->right_glove())) {
				player_glove.Stop();
				return;
		}

		if (opponent_->face().IsOnScreen() && 
			CollisionDetection::CollisionExists(player_glove, opponent_->face())) {
				opponent_->TakeDamage(1);
				player_glove.Stop();
				return;
		}
	}
}

void GameState::HandleOpponentGlovePunching(OpponentGlove& opponent_glove)
{
	if (opponent_glove.IsPunching()) {

		if (player_->left_glove().IsBlocking() &&
				opponent_glove.mesh_instance().transform().GetTranslation().x < -0.02f) {
				opponent_glove.Blocked();
				player_->left_glove().BlockCooldown();
				return;
		}

		if (player_->right_glove().IsBlocking() &&
			opponent_glove.mesh_instance().transform().GetTranslation().x > 0.02f) {
				opponent_glove.Blocked();
				player_->right_glove().BlockCooldown();
				return;
		}

		if (opponent_glove.mesh_instance().transform().GetTranslation().z > -0.2f) {
			opponent_glove.Punch();
			player_->TakeDamage(1);
		} 
	}
}

void GameState::CheckGameOverConditions()
{
	if (player_->health() <= 0)
	{
		ARApp::GetInstance()->TransitionTo(new GameOverState(platform_, false));
	}

	if (opponent_->health() <= 0)
	{
		ARApp::GetInstance()->TransitionTo(new GameOverState(platform_, true));
	}
}

void GameState::DrawHUD()
{
	if(font_.get())
	{
		font_->RenderText(sprite_renderer_.get(), abfw::Vector3(40.0f, 40.0f, -0.9f),
			1.5f, 0xffffffff, abfw::TJ_LEFT, "Health: %d", player_->health());

		font_->RenderText(sprite_renderer_.get(), abfw::Vector3(920.0f, 40.0f, -0.9f),
			1.5f, 0xffffffff, abfw::TJ_RIGHT, "Opponent Health: %d", opponent_->health());

		DrawOpponentMissingText();

		if (render_debug_info_)
			DrawDebugInfo();
	}
}

void GameState::DrawOpponentMissingText()
{
	if (!opponent_->IsOnScreen()) 
	{
		font_->RenderText(sprite_renderer_.get(), abfw::Vector3(480.0f, 200.0f, -0.9f), 
			2.0f, 0xffffffff, abfw::TJ_CENTRE, "Find your");
		font_->RenderText(sprite_renderer_.get(), abfw::Vector3(480.0f, 240.0f, -0.9f), 
			2.0f, 0xffffffff, abfw::TJ_CENTRE, "opponent!");
	}
	else if (!opponent_->GlovesOnScreen()) 
	{
		font_->RenderText(sprite_renderer_.get(), abfw::Vector3(480.0f, 200.0f, -0.9f), 
			2.0f, 0xffffffff, abfw::TJ_CENTRE, "Tell that guy");
		font_->RenderText(sprite_renderer_.get(), abfw::Vector3(480.0f, 240.0f, -0.9f), 
			2.0f, 0xffffffff, abfw::TJ_CENTRE, "to stick 'em up!");
	}
}

void GameState::DrawDebugInfo()
{
		// Render marker positions
	/*for (int i = 0; i < 6; ++i) 
	{
		if (marker_translations_[i].LengthSqr() > 0.001f)
		RenderTranslation(marker_translations_[i]);
	}*/

	font_->RenderText(sprite_renderer_.get(), abfw::Vector3(850.0f, 510.0f, -0.9f), 
		1.0f, 0xffffffff, abfw::TJ_LEFT, "FPS: %.1f", fps_);

	DrawTranslationText(player_->left_glove().mesh_instance().transform().GetTranslation());
	DrawTranslationText(player_->right_glove().mesh_instance().transform().GetTranslation());

	if (opponent_->face().IsOnScreen()) {
		DrawTranslationText(opponent_->face().collision_sphere().position());
	}

	if (opponent_->left_glove().IsOnScreen()) {
		DrawTranslationText(opponent_->left_glove().mesh_instance().transform().GetTranslation());
	}

	if (opponent_->right_glove().IsOnScreen()) {
		DrawTranslationText(opponent_->right_glove().mesh_instance().transform().GetTranslation());
	}
}

void GameState::DrawTranslationText(const abfw::Vector3& translation)
{
	// project the 3d coordinates onto the screen
	abfw::Vector3 pos = translation.Transform(view_matrix_3d_);
	pos = pos.Transform(projection_matrix_3d_);
	pos.x /= pos.z;
	pos.y /= pos.z;
	pos.x = (pos.x + 1.0f) * 960.0f / 2.0f;
	pos.y = (2.0f - (pos.y + 1.0f)) * 544.0f / 2.0f;
	pos.z = -0.9f;

	font_->RenderText(sprite_renderer_.get(), pos, 1.0f, 0xffffffff, abfw::TJ_CENTRE,
		"[%.2f, %.2f, %.2f]", translation.x, translation.y, translation.z);
}

void GameState::SetTextureForModel(abfw::Model* model, const std::string texture_name)
{
	std::vector<abfw::Texture*> textures;

	abfw::PNGLoader png_loader;

	abfw::ImageData image_data;
	png_loader.Load(texture_name.c_str(), platform_, image_data);
	abfw::Texture* texture = platform_.CreateTexture(image_data);
	textures.push_back(texture);

	model->set_textures(textures);
	model->mesh()->GetPrimitive(0)->set_texture(textures[0]);
}
