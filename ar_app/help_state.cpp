#include "help_state.h"

#include <system/platform.h>
#include <graphics/sprite_renderer.h>
#include <graphics/font.h>
#include <input/sony_controller_input_manager.h>
#include <maths/vector3.h>

#include <string>
#include <sstream>

#include "ar_app.h"
#include "menu_state.h"

namespace {
	// Space between lines for the help text in pixels
	const float kLineSpacing = 30.0f;
}

HelpState::HelpState(abfw::Platform& platform)
	: State(platform)
{
}

HelpState::~HelpState()
{
}

bool HelpState::Init()
{
	sprite_renderer_ = ARApp::GetInstance()->sprite_renderer();
	font_ = ARApp::GetInstance()->font();

	return true;
}

bool HelpState::HandleInput(abfw::SonyControllerInputManager* input_manager)
{
	const abfw::SonyController* controller = input_manager->GetController(0);

	if(controller)
	{
		// press any button to return to the menu screen
		if(controller->buttons_pressed())
		{
			ARApp::GetInstance()->TransitionTo(new MenuState(platform_));
		}
	}

	return true;
}

bool HelpState::Update(float frame_time)
{
	return true;
}

bool HelpState::Render()
{
	sprite_renderer_->Begin(true);

	std::string help_text =
		"Instructions for player:\n"
		"    Punch the opponent using the Square and Circle buttons.\n"
		"    Hit the opponents face to reduce their health.\n"
		"    Use the L and R buttons to block punches from the opponent.\n"
		"    Your hand will need to recover for a short period after blocking an attack.\n"
		"\n"
		"Instructions for opponent:\n"
		"    Put on the BoxAR gloves and headgear.\n"
		"    Block punches by holding your fists up.\n"
		"    Punch towards the vita camera to damage the player.\n"
		"\n"
		"Press Select to readjust the camera if it's too bright or too dark.\n"
		"Press Triangle to toggle debug info rendering.";

	size_t lines = std::count(help_text.begin(), help_text.end(), '\n');

	// since abfw::Font::RenderText() does not add new lines, break up 
	// the help string into seperate lines and make seeprate calls to RenderText()

	std::istringstream str_stream(help_text);
	float y = (544.0f - (lines * kLineSpacing)) / 2.0f;
	while (!str_stream.eof())
	{
		std::string str;
		std::getline(str_stream, str);
		font_->RenderText(sprite_renderer_.get(), abfw::Vector3(100.0f, y, -0.9f), 1.0f,
			0xffffffff, abfw::TJ_LEFT, str.c_str());
		y += kLineSpacing;
	}

	sprite_renderer_->End();

	return true;
}
