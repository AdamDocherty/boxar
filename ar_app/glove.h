#ifndef glove_h__
#define glove_h__

#pragma once

#include <graphics/mesh_instance.h>

namespace abfw {
	class Model;
}

class Glove
{
public:
	explicit Glove(abfw::Model& model);
	virtual ~Glove();

	virtual void Update(const float time_elapsed) = 0;

	inline abfw::MeshInstance& mesh_instance() { return mesh_instance_; }
	inline const abfw::MeshInstance& mesh_instance() const { return mesh_instance_; }
protected:
	abfw::MeshInstance mesh_instance_;
};

#endif // glove_h__