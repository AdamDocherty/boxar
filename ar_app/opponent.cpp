#include "opponent.h"

#include <graphics/renderer_3d.h>

#include "audio.h"

namespace {
	// the max health of the opponent, the opponent starts with full health
	const int kMaxHealth = 20;
	// the alpha value of the face when the opponents health is 0
	// at all health values between kMaxHealth and 0, the alpha is interpolated
	// linearly between 0.0f and kMaxFaceAlpha
	const float kMaxFaceAlpha = 1.0f;
}

Opponent::Opponent(abfw::Model& left_glove, abfw::Model& right_golve, abfw::Model& face)
	: left_glove_(left_glove, 1, 0) // use markers 2 and 1 for the left glove
	, right_glove_(right_golve, 3, 2) // use markers 4 and 3 for the right glove
	, face_(face, 4) // user marker 5 for the face
	, health_(kMaxHealth)
{
}

Opponent::~Opponent(void)
{
}

bool Opponent::IsOnScreen() const
{
	return GlovesOnScreen() || face_.IsOnScreen();
}

bool Opponent::GlovesOnScreen() const
{
	return left_glove_.IsOnScreen() || right_glove_.IsOnScreen();
}

void Opponent::Update(const float time_elapsed)
{
	face_.Update(time_elapsed);
	left_glove_.Update(time_elapsed);
	right_glove_.Update(time_elapsed);
}

void Opponent::Render(abfw::Renderer3D* renderer)
{
	if (left_glove_.IsOnScreen()) {
		renderer->DrawMesh(left_glove_.mesh_instance());
	}
	if (right_glove_.IsOnScreen()) {
		renderer->DrawMesh(right_glove_.mesh_instance());
	}

	RenderFace(renderer);
}

void Opponent::RenderFace(abfw::Renderer3D* renderer)
{
	if (face_.IsOnScreen()) {
		float alpha = (1.0f - static_cast<float>(health_) / static_cast<float>(kMaxHealth)) * kMaxFaceAlpha;
		alpha = std::max(std::min(kMaxFaceAlpha, alpha), 0.0f);

		abfw::Colour default_colour = renderer->default_shader_data().ambient_light_colour();
		renderer->default_shader_data().set_ambient_light_colour(abfw::Colour(1.0f, 1.0f, 1.0f, alpha));

		renderer->DrawMesh(face_.mesh_instance());

		renderer->default_shader_data().set_ambient_light_colour(default_colour);
	}
}

void Opponent::TakeDamage(int damage)
{
	health_ -= damage;
	if (damage > 0) {
		Audio::GetInstance().PlaySample("punch");
	}
}
