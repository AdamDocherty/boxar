#ifndef player_glove_h__
#define player_glove_h__

#pragma once

#include "glove.h"

#include <maths/vector3.h>

class PlayerGlove :	public Glove
{
public:
	PlayerGlove(abfw::Model& model, const abfw::Vector3& reseting_position);
	~PlayerGlove();

	// change the state of the glove to `punching'
	void Punch();
	// call Block(true) if the player is currently blocking, Block(false) otherwise
	void Block(bool block);
	// call when the player has hit something to return the glove to the resting position
	void Stop();
	// call when the player has successfully blocked an attack, start the block cooldown
	void BlockCooldown();

	// true if the player is currently punching
	bool IsPunching() const;
	// true if the player is currently blocking
	bool IsBlocking() const;

	virtual void Update(const float time_elapsed);


private:
	void UpdateAnimation(const float time_elapsed);

	// Sets the transformation for punching and blocking animations
	// t is the current time in the animation
	void SetPositionPunching(float t);
	void SetPositionBlocking(float t);
	// Sets the transformation to the resting position
	void SetPositionResting();
	abfw::Vector3 resting_position_;

	// true if the block button is being held down
	bool blocking_;
	// the timer for handling the punch animation
	float punch_timer_;
	// the timer for handling the block animation
	float block_timer_;
	float block_cooldown_timer_;
};

#endif // player_glove_h__
