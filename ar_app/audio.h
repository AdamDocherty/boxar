#ifndef audio_manager_h__
#define audio_manager_h__

#pragma once

namespace abfw {
	class AudioManager;
	class Platform;
}

#include <string>
#include <map>
#include <memory>

class Audio
{
private:
	Audio();
	~Audio();
	Audio(const Audio& copy) = delete;
	Audio operator=(const Audio& copy) = delete;

public:
	static Audio& GetInstance()
	{
		static Audio inst;
		return inst;
	}

	void CleanUp();

	void set_audio_manager(abfw::AudioManager* audio_manager);
	void set_platform(abfw::Platform * platform)
	{
		platform_ = platform;
	}

	// plays an audio sample, if the sample has not been loaded
	// attempts to load the sample
	void PlaySample(const std::string& name);

private:
	// loads an audio sample, pass in the name of the audio without
	// the file path or file extension
	int LoadSample(const std::string& name);

	// map of sample names to sound ids
	std::map<std::string, int> sample_map_;

	std::unique_ptr<abfw::AudioManager> audio_manager_;
	abfw::Platform * platform_;
};

#endif // audio_h__
