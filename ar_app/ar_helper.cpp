#include "ar_helper.h"

#include <sony_sample_framework.h>
#include <sony_tracking.h>

#include <maths/vector3.h>

#include "constants.h"

bool ARHelper::IsMarkerOnScreen(const int marker_id)
{
	return sampleIsMarkerFound(marker_id);
}

bool ARHelper::GetMarkerTransform(abfw::Matrix44& marker_transform, 
	const int marker_id, float marker_size /* = 0.059f */)
{
	if (IsMarkerOnScreen(marker_id))
	{
		sampleGetTransform(marker_id, &marker_transform);

		// scale the marker matrix if the marker size is not the default size
		if (marker_size != Constants::kSonyDefaultMarkerSize) {
			float sf = marker_size / Constants::kSonyDefaultMarkerSize;
			abfw::Matrix44 scale_matrix;
			scale_matrix.SetIdentity();
			scale_matrix.Scale(abfw::Vector3(sf, sf, sf));
			marker_transform = marker_transform * scale_matrix;
		}
		return true;
	}
	else
	{
		return false;
	}
}
