#ifndef game_state_h__
#define game_state_h__

#pragma once

#include "state.h"

#include <graphics/sprite.h>
#include <graphics/mesh_instance.h>
#include <graphics/vita/texture_vita.h>
#include <graphics/model.h>

// Vita AR includes
#include <camera.h>
#include <gxm.h>
#include <motion.h>
#include <libdbg.h>
#include <libsmart.h>

// language includes
#include <vector>
#include <memory>
#include <map>

// FRAMEWORK FORWARD DECLARATIONS
namespace abfw
{
	class Platform;
	class SpriteRenderer;
	class Font;
	class Renderer3D;
	class Model;
	class RenderTarget;
	class TextureVita;
	class SonyControllerInputManager;
}

// gameplay forward declarations
class Player;
class PlayerGlove;
class Opponent;
class OpponentGlove;

namespace abfw {
	class Font;
}

// gameplay declarations
class Player;
class PlayerGlove;
class Opponent;
class OpponentGlove;

class GameState : public State
{
public:
	explicit GameState(abfw::Platform& platform);
	virtual ~GameState();

	virtual bool Init();
	virtual bool HandleInput(abfw::SonyControllerInputManager* input_manager);
	virtual bool Update(float frame_time);
	virtual bool Render();
private:
	void InitFont();
	void Init3dProjectionMatrix();
	void InitCameraFeed();
	void LoadModels();
	void SetTextureForModel(abfw::Model* model, const std::string texture_name);

	bool UpdateAR(float frame_time);

	void HandleCollisions();
	void HandlePlayerGlovePunching(PlayerGlove& player_glove);
	void HandleOpponentGlovePunching(OpponentGlove& opponent_glove);

	void CheckGameOverConditions();

	bool Render3D();
	bool RenderSprites();
	void RenderCameraFeed(struct AppData* dat);
	void DrawHUD();
	void DrawOpponentMissingText();
	void DrawDebugInfo();
	void DrawTranslationText(const abfw::Vector3& translation);

	std::shared_ptr<abfw::SpriteRenderer> sprite_renderer_;
	std::shared_ptr<abfw::Renderer3D> renderer_3d_;

	// Camera Feed Variables
	abfw::Matrix44 camera_feed_matrix_;
	abfw::Sprite camera_feed_sprite_;
	static std::unique_ptr<abfw::TextureVita> s_camera_feed_texture;

	// Camera Variables
	abfw::Matrix44 projection_matrix_3d_;
	abfw::Matrix44 view_matrix_3d_;

	std::map<std::string, abfw::Model> models_;
	std::shared_ptr<abfw::Font> font_;

	std::unique_ptr<Player> player_;
	std::unique_ptr<Opponent> opponent_;

	float fps_;
	bool render_debug_info_;
};

#endif // game_state_h__
