#include "opponent_glove.h"

#include <graphics/model.h>
#include <graphics/mesh.h>

#include "constants.h"
#include "ar_helper.h"

namespace {
	// The number of frames the opponent must be moving their first forawrd for it
	// to be considererd a punch
	const size_t kNumFramesForPunch = 5;
	// The threshold speed that the opponent must be punching at
	// in meters per frame
	const float kPunchSpeedRequired = 0.01f;

	const float kPunchCooldownTime = 0.5f;
}

OpponentGlove::OpponentGlove(abfw::Model& model, 
	int marker_index_attacking, int marker_index_defending)
	: Glove(model)
	, marker_index_attacking_(marker_index_attacking)
	, marker_index_defending_(marker_index_defending)
	, attacking_(false)
	, defending_(false)
	, punch_cooldown_timer_(0.0f)
{
	previous_positions_.resize(kNumFramesForPunch);
}

OpponentGlove::~OpponentGlove()
{
}

void OpponentGlove::Update(const float time_elapsed)
{
	if (punch_cooldown_timer_ > 0.0f)
		punch_cooldown_timer_ -= time_elapsed;

	GetStateFromMarkers();
}

void OpponentGlove::Punch()
{
	punch_cooldown_timer_ = kPunchCooldownTime;
	previous_positions_.clear();
}

void OpponentGlove::SavePosition(const abfw::Vector3& pos)
{
	if (previous_positions_.size() >= kNumFramesForPunch)
		previous_positions_.pop_back();
	previous_positions_.push_front(pos);
}

void OpponentGlove::Blocked()
{
	previous_positions_.clear();
}

bool OpponentGlove::IsPunching()
{
	if (!IsAttacking()) return false;
	if (punch_cooldown_timer_ > 0.0f) return false;
	if (previous_positions_.size() < kNumFramesForPunch) return false;

	// if the glove has been moving towards the camera for the last kNumFramesForPunch frames
	for (unsigned int i = 0; i < kNumFramesForPunch - 1; ++i)
	{
		abfw::Vector3 v = previous_positions_[i] - previous_positions_[i + 1];
		if (v.z < kPunchSpeedRequired) return false;
	}

	return true;
}

void OpponentGlove::GetStateFromMarkers()
{
	attacking_ = false;
	defending_ = false;

	abfw::Matrix44 marker_transform;
	if (ARHelper::GetMarkerTransform(marker_transform, marker_index_attacking_, Constants::kMarkerSize)) {
		SetAttacking(marker_transform);
	}
	else if (ARHelper::GetMarkerTransform(marker_transform, marker_index_defending_, Constants::kMarkerSize)) {
		SetDefending(marker_transform);
	}
	else {
		previous_positions_.clear();
	}
}

void OpponentGlove::SetAttacking(abfw::Matrix44& marker_transform)
{
	attacking_ = true;

	// rotate the matrix to be in the Attack position
	abfw::Matrix44 rotation_matrix;
	rotation_matrix.RotationX(1.57079632679f);
	abfw::Matrix44 mesh_matrix = rotation_matrix * marker_transform;

	// Move the matrix so that the front of the virtual glove matches the front of the real glove
	abfw::Vector3 translation = mesh_matrix.GetTranslation();
	abfw::Vector3 axis = abfw::Vector3(mesh_matrix.m[1][0], mesh_matrix.m[1][1], mesh_matrix.m[1][2]);
	translation -= axis * mesh_instance_.mesh()->aabb().max_vtx().y;
	mesh_matrix.SetTranslation(translation);

	SavePosition(translation);

	mesh_instance_.set_transform(mesh_matrix);
}

void OpponentGlove::SetDefending(abfw::Matrix44& marker_transform)
{
	defending_ = true;

	// rotate the matrix to be in the defend position
	abfw::Matrix44 rotation_matrix;
	rotation_matrix.RotationY(3.14159265359f);
	abfw::Matrix44 mesh_matrix = rotation_matrix * marker_transform;

	// Move the matrix so that the front of the virtual glove matches the front of the real glove
	abfw::Vector3 translation = mesh_matrix.GetTranslation();
	abfw::Vector3 axis = abfw::Vector3(mesh_matrix.m[2][0], mesh_matrix.m[2][1], mesh_matrix.m[2][2]);
	translation += axis * mesh_instance_.mesh()->aabb().max_vtx().z;
	mesh_matrix.SetTranslation(translation);

	previous_positions_.clear();

	mesh_instance_.set_transform(mesh_matrix);
}