#ifndef help_state_h__
#define help_state_h__

#pragma once

#include "state.h"

#include <memory>

namespace abfw {
	class SpriteRenderer;
	class Font;
}

class HelpState : public State
{
public:
	explicit HelpState(abfw::Platform& platform);
	~HelpState();

	virtual bool Init();
	virtual bool HandleInput(abfw::SonyControllerInputManager* input_manager);
	virtual bool Update(float frame_time);
	virtual bool Render();
private:

	std::shared_ptr<abfw::SpriteRenderer> sprite_renderer_;
	std::shared_ptr<abfw::Font> font_;

};

#endif // help_state_h__