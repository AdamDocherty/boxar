#pragma once

namespace abfw {
	class Matrix44;
}

class ARHelper
{
private:
	// Make this a static class
	ARHelper() = delete;
	~ARHelper() = delete;
	ARHelper(const ARHelper& copy) = delete;
	ARHelper operator=(const ARHelper& copy) = delete;

public:
	// returns true if marker with marker_id is on screen
	static bool IsMarkerOnScreen(const int marker_id);
	// gets the marker transform for the marker with marker_id
	// does not update the matrix if the marker is not on screen
	// set marker_size to the size of the marker you are using in meters
	static bool GetMarkerTransform(abfw::Matrix44& marker_transform, 
		const int marker_id, float marker_size /* = 0.059f */);
};

