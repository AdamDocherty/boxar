#include "procedural_object_generator.h"

#include <system/platform.h>
#include <graphics/mesh.h>
#include <graphics/primitive.h>

// sorry for abusing macros

// dirty technique to add a box with no uv coordinates to a vetrex list
#define BOX_VERTICIES(centreX, centreY, centreZ, x, y, z) \
{abfw::Vector3((centreX) + (x)*0.5f, (centreY) - (y)*0.5f, (centreZ) - (z)*0.5f), abfw::Vector3(normal, -normal, -normal), abfw::Vector2(0.0f, 0.0f)}, \
{abfw::Vector3((centreX) + (x)*0.5f, (centreY) + (y)*0.5f, (centreZ) - (z)*0.5f), abfw::Vector3(normal, normal, -normal), abfw::Vector2(0.0f, 0.0f)}, \
{abfw::Vector3((centreX) - (x)*0.5f, (centreY) + (y)*0.5f, (centreZ) - (z)*0.5f), abfw::Vector3(-normal, normal, -normal), abfw::Vector2(0.0f, 0.0f)}, \
{abfw::Vector3((centreX) - (x)*0.5f, (centreY) - (y)*0.5f, (centreZ) - (z)*0.5f), abfw::Vector3(-normal, -normal, -normal), abfw::Vector2(0.0f, 0.0f)}, \
{abfw::Vector3((centreX) + (x)*0.5f, (centreY) - (y)*0.5f, (centreZ) + (z)*0.5f), abfw::Vector3(normal, -normal, normal), abfw::Vector2(0.0f, 0.0f)}, \
{abfw::Vector3((centreX) + (x)*0.5f, (centreY) + (y)*0.5f, (centreZ) + (z)*0.5f), abfw::Vector3(normal, normal, normal), abfw::Vector2(0.0f, 0.0f)}, \
{abfw::Vector3((centreX) - (x)*0.5f, (centreY) + (y)*0.5f, (centreZ) + (z)*0.5f), abfw::Vector3(-normal, normal, normal), abfw::Vector2(0.0f, 0.0f)}, \
{abfw::Vector3((centreX) - (x)*0.5f, (centreY) - (y)*0.5f, (centreZ) + (z)*0.5f), abfw::Vector3(-normal, -normal, normal), abfw::Vector2(0.0f, 0.0f)}

// another dirty technique to add the box indicies to the index list
#define BOX_INDICIES(box) \
	(8 * (box)) + 0, (8 * (box)) + 1, (8 * (box)) + 2, \
	(8 * (box)) + 2, (8 * (box)) + 3, (8 * (box)) + 0, \
	(8 * (box)) + 6, (8 * (box)) + 5, (8 * (box)) + 4, \
	(8 * (box)) + 4, (8 * (box)) + 7, (8 * (box)) + 6, \
	(8 * (box)) + 2, (8 * (box)) + 7, (8 * (box)) + 3, \
	(8 * (box)) + 2, (8 * (box)) + 6, (8 * (box)) + 7, \
	(8 * (box)) + 0, (8 * (box)) + 4, (8 * (box)) + 1, \
	(8 * (box)) + 5, (8 * (box)) + 1, (8 * (box)) + 4, \
	(8 * (box)) + 6, (8 * (box)) + 2, (8 * (box)) + 1, \
	(8 * (box)) + 5, (8 * (box)) + 6, (8 * (box)) + 1, \
	(8 * (box)) + 0, (8 * (box)) + 3, (8 * (box)) + 7, \
	(8 * (box)) + 0, (8 * (box)) + 7, (8 * (box)) + 4 

abfw::Mesh* ProceduralObjectGenerator::CreateGloveMesh(abfw::Platform& platform, const float width, 
	const float height, const float depth, const int side)
{
	abfw::Mesh* mesh = platform.CreateMesh();
	const float normal = 0.577f;
	const float thumbWidth = width*0.75f;
	const float thumbHeight = height*0.6f;
	const float thumbDepth = depth*0.33f;
	const float thumbX = (side < 0) ? (-width*0.5f) : (width*0.5f);

	const abfw::Mesh::Vertex vertices[] = {
		BOX_VERTICIES(0.0f  , 0.0f        , 0.0f, width, height    , depth),					// hand
		BOX_VERTICIES(thumbX, 0.0f        , depth*0.08f, thumbWidth, thumbHeight, thumbDepth),	// thumb
		BOX_VERTICIES(0.0f  , -height*0.7f, 0.0f       , width*0.6f, height*0.4f, depth*0.6f),	// wrist
		BOX_VERTICIES(0.0f  , height*0.45f, depth*0.6f , width     , height*0.2f, depth*0.2f)	// fingertips
	};

	mesh->InitVertexBuffer(platform, static_cast<const void*>(vertices), sizeof(vertices)/sizeof(abfw::Mesh::Vertex), sizeof(abfw::Mesh::Vertex));

	mesh->AllocatePrimitives(1);
	abfw::Primitive* primitive = mesh->GetPrimitive(0);

	const UInt32 indices[] = {
		BOX_INDICIES(0),
		BOX_INDICIES(1),
		BOX_INDICIES(2),
		BOX_INDICIES(3)
	};

	primitive->InitIndexBuffer(platform, static_cast<const void*>(indices), sizeof(indices)/sizeof(UInt32),sizeof(UInt32));
	primitive->SetType(abfw::TRIANGLE_LIST);

	mesh->set_aabb(abfw::Aabb(
		abfw::Vector3(-width*0.5f, -height*0.5f, -depth*0.5f), 
		abfw::Vector3( width*0.5f,  height*0.5f,  depth*0.5f)));

	return mesh;
}

abfw::Mesh* ProceduralObjectGenerator::CreatePlaneMesh(abfw::Platform& platform, 
	const float width, const float height)
{
	abfw::Mesh* mesh = platform.CreateMesh();
	const float normal = 0.577f;

	const abfw::Mesh::Vertex vertices[] = {
		{abfw::Vector3(width*0.5f, height*0.5f, 0.0f), abfw::Vector3(normal, -normal, -normal), abfw::Vector2(1.0f, 0.0f)},
		{abfw::Vector3(-width*0.5f, height*0.5f, 0.0f), abfw::Vector3(normal, -normal, -normal), abfw::Vector2(0.0f, 0.0f)},
		{abfw::Vector3(-width*0.5f, -height*0.5f, 0.0f), abfw::Vector3(normal, -normal, -normal), abfw::Vector2(0.0f, 1.0f)},
		{abfw::Vector3(width*0.5f, -height*0.5f, 0.0f), abfw::Vector3(normal, -normal, -normal), abfw::Vector2(1.0f, 1.0f)}
	};

	mesh->InitVertexBuffer(platform, static_cast<const void*>(vertices), sizeof(vertices)/sizeof(abfw::Mesh::Vertex), sizeof(abfw::Mesh::Vertex));

	mesh->AllocatePrimitives(1);
	abfw::Primitive* primitive = mesh->GetPrimitive(0);

	const UInt32 indices[] = {
		0, 1, 2,
		2, 3, 0
	};

	primitive->InitIndexBuffer(platform, static_cast<const void*>(indices), sizeof(indices)/sizeof(UInt32),sizeof(UInt32));
	primitive->SetType(abfw::TRIANGLE_LIST);

	mesh->set_aabb(abfw::Aabb(
		abfw::Vector3(-width*0.5f, -height*0.5f, -width*0.5f), 
		abfw::Vector3( width*0.5f,  height*0.5f,  width*0.5f)));

	return mesh;
}

#undef BOX_VERTICIES
#undef BOX_INDICIES