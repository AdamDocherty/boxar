#pragma once

#include "player_glove.h"

namespace abfw {
	class SonyController;
	class Renderer3D;
}

class Opponent;

class Player
{
public:
	Player(abfw::Model& left_glove, abfw::Model& right_glove);
	~Player();

	void Update(float elapsed_time);
	void HandleInput(const abfw::SonyController* controller);
	void Render(abfw::Renderer3D* renderer);

	PlayerGlove& left_glove() { return left_glove_; }
	PlayerGlove& right_glove() { return right_glove_; }

	int health() const { return health_; }

	// reduces the players health and plays a sound
	void TakeDamage(int damage);
private:
	PlayerGlove left_glove_;
	PlayerGlove right_glove_;

	int health_;
};

