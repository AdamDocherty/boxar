#ifndef opponent_h__
#define opponent_h__

#pragma once

#include "opponent_glove.h"
#include "opponent_face.h"

namespace abfw {
	class Renderer3D;
}

class Opponent
{
public:
	Opponent(abfw::Model& left_glove, abfw::Model& right_glove, abfw::Model& face);
	~Opponent();

	// true if any part of the opponent (either glove, or face) is on screen
	bool IsOnScreen() const;
	// true only if the gloves are on screen
	bool GlovesOnScreen() const;

	void Update(const float time_elapsed);
	void Render(abfw::Renderer3D* renderer);

	// reduces the opponents health
	void TakeDamage(int damage);

	OpponentFace& face() { return face_; }
	OpponentGlove& left_glove() { return left_glove_; }
	OpponentGlove& right_glove() { return right_glove_; }

	int health() const { return health_; }

private:
	// renders the face, the face is partially transparent
	// based on the current health of the opponent
	void RenderFace(abfw::Renderer3D* renderer);

	OpponentGlove left_glove_;
	OpponentGlove right_glove_;
	OpponentFace face_;

	int health_;
};
#endif // opponent_h__

