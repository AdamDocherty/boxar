#include "player.h"

#include <input/sony_controller_input_manager.h>
#include <graphics/renderer_3d.h>

#include "audio.h"

#include "opponent.h"
#include "collision_detection.h"

namespace {
	const int kMaxHealth = 10;
}

Player::Player(abfw::Model& left_glove, abfw::Model& right_glove)
	: left_glove_(left_glove, abfw::Vector3(-0.05f, -0.1f, -0.05f))
	, right_glove_(right_glove, abfw::Vector3(0.05f, -0.1f, -0.05f))
	, health_(kMaxHealth)
{
}

Player::~Player(void)
{
}

void Player::Update(float elapsed_time)
{
	left_glove_.Update(elapsed_time);
	right_glove_.Update(elapsed_time);
}

void Player::HandleInput(const abfw::SonyController* controller)
{
	// Punching with the left hand
	if (controller->buttons_pressed() & ABFW_SONY_CTRL_SQUARE)
		left_glove_.Punch();
	// Punching with the right hand
	if (controller->buttons_pressed() & ABFW_SONY_CTRL_CIRCLE)
		right_glove_.Punch();

	// Blocking with the left hand
	bool left_block = controller->buttons_down() & ABFW_SONY_CTRL_L2;
	left_glove_.Block(left_block);
	// Blocking with the right hand
	bool right_block = controller->buttons_down() & ABFW_SONY_CTRL_R2;
	right_glove_.Block(right_block);
}

void Player::Render(abfw::Renderer3D* renderer)
{
	renderer->DrawMesh(left_glove_.mesh_instance());
	renderer->DrawMesh(right_glove_.mesh_instance());
}

void Player::TakeDamage(int damage)
{
	health_ -= damage;
	if (damage > 0) {
		Audio::GetInstance().PlaySample("hurt");
	}
}
