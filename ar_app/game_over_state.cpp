#include "game_over_state.h"

#include <system/platform.h>
#include <graphics/sprite_renderer.h>
#include <graphics/font.h>
#include <input/sony_controller_input_manager.h>
#include <maths/vector3.h>

#include "ar_app.h"
#include "menu_state.h"

GameOverState::GameOverState(abfw::Platform& platform, bool win)
	: State(platform)
	, win_(win)
{
}

GameOverState::~GameOverState()
{
}

bool GameOverState::Init()
{
	sprite_renderer_ = ARApp::GetInstance()->sprite_renderer();
	font_ = ARApp::GetInstance()->font();

	return true;
}

bool GameOverState::HandleInput(abfw::SonyControllerInputManager* input_manager)
{
	const abfw::SonyController* controller = input_manager->GetController(0);

	if(controller)
	{
		// press any button to return to the menu screen
		if(controller->buttons_pressed())
		{
			ARApp::GetInstance()->TransitionTo(new MenuState(platform_));
		}
	}

	return true;
}

bool GameOverState::Update(float frame_time)
{
	return true;
}

bool GameOverState::Render()
{
	abfw::Matrix44 proj_matrix2d;
	proj_matrix2d.OrthographicFrustumVita(0.0f, platform_.width(), 0.0f, platform_.height(), -1.0f, 1.0f);
	sprite_renderer_->set_projection_matrix(proj_matrix2d);
	sprite_renderer_->Begin(true);

	if (win_)
	{
		font_->RenderText(sprite_renderer_.get(), abfw::Vector3(480.0f, 100.0f, -0.9f), 2.0f,
			0xffffffff, abfw::TJ_CENTRE, "Congratulations! You Won.");
	}
	else
	{
		font_->RenderText(sprite_renderer_.get(), abfw::Vector3(480.0f, 100.0f, -0.9f), 2.0f,
			0xffffffff, abfw::TJ_CENTRE, "You lost, but that's OK.");
		font_->RenderText(sprite_renderer_.get(), abfw::Vector3(480.0f, 260.0f, -0.9f), 1.0f,
			0xffffffff, abfw::TJ_CENTRE, "It ain't how hard you hit...");
		font_->RenderText(sprite_renderer_.get(), abfw::Vector3(480.0f, 290.0f, -0.9f), 1.0f,
			0xffffffff, abfw::TJ_CENTRE, "It's how hard you can get hit and keep moving forward.");
		 
	}

	sprite_renderer_->End();

	return true;
}
