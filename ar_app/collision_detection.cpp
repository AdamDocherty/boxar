#include "collision_detection.h"

#include "opponent_face.h"
#include "glove.h"

#include <maths/sphere.h>

bool CollisionDetection::SpheresCollide(const abfw::Sphere& a, const abfw::Sphere& b)
{
	// basic sphere collision
	abfw::Vector3 d = a.position() - b.position();
	float distance = d.Length();
	float sum_radius = a.radius() + b.radius();
	return distance < sum_radius;
}

bool CollisionDetection::CollisionExists(const OpponentFace& opponent_face, const Glove& glove)
{
	// 0.06f is an arbritary size for the gloves
	// TODO: use the mesh to get the radius of the sphere
	abfw::Sphere glove_sphere(glove.mesh_instance().transform().GetTranslation(), 0.06f);
	return SpheresCollide(glove_sphere, opponent_face.collision_sphere());
}

bool CollisionDetection::CollisionExists(const Glove& glove, const OpponentFace& opponent_face)
{
	return CollisionExists(opponent_face, glove);
}

bool CollisionDetection::CollisionExists(const Glove& gloveA, const Glove& gloveB)
{
	abfw::Sphere a(gloveA.mesh_instance().transform().GetTranslation(), 0.06f);
	abfw::Sphere b(gloveB.mesh_instance().transform().GetTranslation(), 0.06f);
	return SpheresCollide(a, b);
}
