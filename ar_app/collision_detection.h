#pragma once

namespace abfw {
	class Sphere;
}

class Glove;
class OpponentFace;

class CollisionDetection
{
private:
	// Make this a static class
	CollisionDetection() = delete;
	~CollisionDetection() = delete;
	CollisionDetection(const CollisionDetection& copy) = delete;
	CollisionDetection operator=(const CollisionDetection& copy) = delete;

public:
	// returns true if two spheres collide
	static bool SpheresCollide(const abfw::Sphere& a, const abfw::Sphere& b);

	// returns true if the opponents_face collides with the glove
	// uses sphere collision, spheres for the gloves are generated based on the position
	static bool CollisionExists(const OpponentFace& opponent_face, const Glove& glove);
	static bool CollisionExists(const Glove& glove, const OpponentFace& opponent_face);

	// returns true if both gloves collide with each other
	// uses sphere collision, spheres for the gloves are generated based on the position
	static bool CollisionExists(const Glove& gloveA, const Glove& gloveB);
};

